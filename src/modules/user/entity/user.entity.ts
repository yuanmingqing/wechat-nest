import * as dayjs from 'dayjs';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne } from 'typeorm';
import { FriendEntity } from '@modules/friend/entity/friend.entity';
// import { Exclude } from 'class-transformer';

@Entity({
  name: 'users',
})
export class UserEntity {
  @PrimaryGeneratedColumn({
    comment: '用户id',
  })
  userId: number;

  @Column({
    length: 50,
    nullable: false, // 不能为null
    comment: '用户名',
  })
  username: string;

  @Column({
    length: 20,
    nullable: false, // 不能为null
    comment: '账户',
  })
  account: string;

  @Column({
    length: 20,
    nullable: false, // 不能为null
    comment: '邮箱',
  })
  email: string;

  @Column({
    length: 20,
    nullable: false, // 不能为null
    comment: '密码',
  })
  password: string;

  @Column({
    nullable: false, // 不能为null
    comment: '性别',
  })
  sex: number;

  @Column({
    length: 20,
    nullable: false, // 不能为null
    comment: '生日',
  })
  birth: string;

  @Column({
    length: 20, // 长度
    nullable: false, // 不能为null
    default: '', // 默认值
    comment: '电话', // 描述
  })
  phone: string;

  @Column({
    length: 50,
    nullable: false, // 不能为null
    default: '这家伙啥也没有', // 默认值
    comment: '介绍描述',
  })
  desc: string;

  @Column({
    length: 100,
    nullable: false, // 不能为null
    default: '', // 默认值
    comment: '用户头像',
  })
  imgUrl: string;

  @Column({
    length: 20,
    nullable: false, // 不能为null
    default: dayjs().format('YYYY-MM-DD HH:mm:ss'), // 默认值
    comment: '注册时间',
  })
  createTime: string;

  @Column({
    length: 20,
    nullable: false, // 不能为null
    default: dayjs().format('YYYY-MM-DD HH:mm:ss'), // 默认值
    comment: '更新时间',
  })
  updateTime: string;

  @OneToOne(() => FriendEntity, (friendApply) => friendApply.users)
  friends: FriendEntity;
  status: number; // 0: 不是好友 1:好友 2 代表自己
}
