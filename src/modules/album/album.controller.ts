import { Controller, Get, Post, Body, UseGuards, Req, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AlbumService } from './album.service';
import { CreateAlbumDto, SavePhoneDto, UpdatePhoneDto } from './dto/index';

@Controller('album')
@UseGuards(AuthGuard('jwt'))
export class AlbumController {
  constructor(private readonly albumService: AlbumService) {}
  /**
   * 创建相册
   */
  @Post('createAlbum')
  createAlbum(@Req() request, @Body() body: CreateAlbumDto) {
    const accessToken = request.get('Authorization');
    return this.albumService.createAlbum(accessToken, body);
  }
  /**
   * 相册 删除照片
   */
  @Post('deleteAlbum')
  deleteAlbum(@Req() request, @Body() body: { id: number }) {
    const accessToken = request.get('Authorization');
    return this.albumService.deleteAlbum(accessToken, body);
  }

  /**
   * 相册 批量删除照片
   */
  @Post('batchDeletePhone')
  batchDeletePhone(@Req() request, @Body() body: { id: string }) {
    const accessToken = request.get('Authorization');
    return this.albumService.batchDeletePhone(accessToken, body);
  }

  /**
   * 修改相册
   */
  @Post('updateAlbum')
  updateAlbum(@Req() request, @Body() body: CreateAlbumDto) {
    const accessToken = request.get('Authorization');
    return this.albumService.updateAlbum(accessToken, body);
  }

  /**
   * 相册列表
   */
  @Get('albumList')
  albumList(@Req() request) {
    const accessToken = request.get('Authorization');
    return this.albumService.albumList(accessToken);
  }

  /**
   * 获取 相册详情
   */
  @Get('albumDetails')
  albumDetails(@Req() request, @Query() query: { id: number }) {
    const accessToken = request.get('Authorization');
    return this.albumService.albumDetails(accessToken, query);
  }

  /**
   * 相册 保存照片
   */
  @Post('savePhone')
  savePhone(@Req() request, @Body() body: SavePhoneDto) {
    const accessToken = request.get('Authorization');
    return this.albumService.savePhone(accessToken, body);
  }

  /**
   * 相册 删除照片
   */
  @Post('deletePhone')
  deletePhone(@Req() request, @Body() body: { id: number }) {
    const accessToken = request.get('Authorization');
    return this.albumService.deletePhone(accessToken, body);
  }

  /**
   * 相册 修改照片
   */
  @Post('updatePhone')
  updatePhone(@Req() request, @Body() body: UpdatePhoneDto) {
    const accessToken = request.get('Authorization');
    return this.albumService.updatePhone(accessToken, body);
  }

  /**
   * 相册 获取照片列表
   */
  @Get('phoneList')
  phoneList(@Req() request, @Query() query: { id: number }) {
    const accessToken = request.get('Authorization');
    return this.albumService.phoneList(accessToken, query);
  }
}
