import { Controller, Get, Query } from '@nestjs/common';
import {
  personalized,
  personalized_newsong,
  personalized_mv,
  personalized_djprogram,
  personalized_privatecontent,
  personalized_privatecontent_list,
} from 'NeteaseCloudMusicApi';

@Controller('music/personalized')
export class MusicPersonalizedController {
  constructor() {}
  /**
   * 推荐歌单
   * @param query pageSize 返回数量
   * @returns
   */
  @Get('/')
  async getIndex(@Query() query: { pageSize: number | string }) {
    const { body } = await personalized({ limit: query.pageSize || 10 });
    return { code: body.code, msg: '推荐歌单', data: body.result || [] };
  }

  /**
   * 推荐 mv
   * @param
   * @returns
   */
  @Get('/mv')
  async getMv() {
    const { body } = await personalized_mv({});
    return { code: body.code, msg: '推荐 mv', data: body.result || [] };
  }

  /**
   * 推荐新音乐
   * @param query area pageSize 返回数量
   * @returns
   */
  @Get('/newsong')
  async getNewsong(@Query() query: { pageSize?: string | number; area?: string | number }) {
    const { body } = await personalized_newsong({ limit: query.pageSize, area: query.area });
    return { code: body.code, msg: '推荐新音乐', data: body.result };
  }

  /**
   * 推荐电台
   * @param
   * @returns
   */
  @Get('/djprogram')
  async getDjprogram() {
    const { body } = await personalized_djprogram({});
    return { code: body.code, msg: '推荐电台', data: body.result };
  }

  /**
   * 独家放送(入口列表)
   * @param
   * @returns
   */
  @Get('/privatecontent')
  async gePrivatecontent() {
    const { body } = await personalized_privatecontent({});
    return { code: body.code, msg: '独家放送(入口列表)', data: body.result };
  }

  /**
   * 独家放送列表
   * @param  pageSize?: string | number page?: string | number
   * @returns
   */
  @Get('/privatecontent/list')
  async gePrivatecontentList(@Query() query: { page?: string | number; pageSize?: string | number }) {
    const { body } = await personalized_privatecontent_list({ limit: query.pageSize, offset: query.page });
    return { code: body.code, msg: '独家放送列表', data: body.result };
  }
}
