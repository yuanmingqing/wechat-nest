import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '@auth/index';

import { FriendApplyEntity } from '@modules/friend/entity/friend.apply.entity';
import { FriendEntity } from '@modules/friend/entity/friend.entity';
import { UserEntity } from '@modules/user/entity/user.entity';
import { FriendController } from './friend.controller';
import { FriendService } from './friend.service';

@Module({
  imports: [TypeOrmModule.forFeature([FriendApplyEntity, FriendEntity, UserEntity]), AuthModule],
  controllers: [FriendController],
  providers: [FriendService],
  exports: [FriendService],
})
export class FriendModule {}
