import { Controller, Get, UseGuards, Req, Post, Query, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { banner, mv_url, song_url, song_detail } from 'NeteaseCloudMusicApi';
import { MusicService } from './music.service';

export const enum SoundQualityType {
  standard = 'standard',
  exhigh = 'exhigh',
  lossless = 'lossless',
  hires = 'hires',
}

@Controller('music')
export class MusicController {
  constructor(private readonly musicService: MusicService) {}

  /**
   * 获取banner
   * @param query type: 0:pc; 1:android 2:iphone 3:ipad
   * @returns
   */
  @Get('getBanner')
  async getBanner(@Query() query: { type: number }) {
    const { body } = await banner({ type: query.type });
    return { code: body.code, msg: '获取banner', data: body.banners || [] };
  }

  /**
   * mv 地址
   * @param id?: string | number; r?: string | number
   * @returns
   */
  @Get('mv/url')
  async getMvUrl(@Query() query: { id?: string | number; r?: string | number }) {
    const { body } = await mv_url({ id: query.id, r: query.r });
    return { code: body.code, msg: '获取mv 地址', data: body.data || [] };
  }

  /**
   * 获取音乐 url
   * @param id?: string | number; r?: string | number
   * @returns
   */
  @Get('/song/url')
  async getSongUrl(@Query() query: { id: string | number; br?: string | number }) {
    const { body } = await song_url({ id: query.id, br: query.br });
    return { code: body.code, msg: '获取音乐 url', data: body.data || [] };
  }

  /**
   * 获取歌曲详情
   * @param ids: string
   * @returns
   */

  @Get('/song/detail')
  async getSongDetail(@Query() query: { ids: string }) {
    const { body } = await song_detail({ ids: query.ids });
    console.log(body);
    return { code: body.code, msg: '获取歌曲详情', data: body || [] };
  }
}
