import { Controller, Get, UseGuards, Req, Post, Query, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { query, Request, Response } from 'express';

import { ApplyFriendDTO } from '@modules/friend/dto/index';
import { FriendService } from './friend.service';

@Controller('friend')
export class FriendController {
  constructor(private readonly friendService: FriendService) {}

  /**
   * 申请添加好友
   */
  @UseGuards(AuthGuard('jwt'))
  @Post('/applyFriend')
  async applyFriend(@Body() applyFriend: ApplyFriendDTO) {
    return this.friendService.applyFriend(applyFriend);
  }

  /**
   * 获取申请好友列表
   */
  @UseGuards(AuthGuard('jwt'))
  @Get('/getApplyFriendList')
  async getApplyFriendList(@Req() request: Response) {
    const accessToken = request.get('Authorization');
    const result = await this.friendService.getApplyFriendList(accessToken);
    return result;
  }
  /**
   * 更新好友状态
   */
  @UseGuards(AuthGuard('jwt'))
  @Post('/updateFriendStatus')
  async updateFriendStatus(@Req() request: Response, @Body() body: ApplyFriendDTO) {
    const accessToken = request.get('Authorization');
    return this.friendService.updateFriendStatus(accessToken, body);
  }

  /**
   * 获取好友列表
   */
  @UseGuards(AuthGuard('jwt'))
  @Get('/getFriendList')
  async getFriendList(@Req() request: Response) {
    const accessToken = request.get('Authorization');
    return this.friendService.getFriendList(accessToken);
  }

  /**
   * 获取好友详情
   */
  @UseGuards(AuthGuard('jwt'))
  @Get('/getFriendDetails')
  async getFriendDetails(@Query() query: { id: number }) {
    return this.friendService.getFriendDetails(query.id);
  }

  /**
   * 删除好友
   */
  @UseGuards(AuthGuard('jwt'))
  @Post('/deleteFriend')
  async deleteFriend(@Req() request: Response, @Body() body: { id: number }) {
    const accessToken = request.get('Authorization');

    return this.friendService.deleteFriend(accessToken, body.id);
  }
}
