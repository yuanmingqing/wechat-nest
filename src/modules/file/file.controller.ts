import { Controller, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
// import { Express } from 'express';
// import dayjs from 'dayjs';
// import fs from 'fs';
// import { join } from 'path';
import config from '@config/config.default';

@Controller('/upload')
export class FileController {
  constructor() {}
  /**
   * 单张上传图片
   */
  @UseGuards(AuthGuard('jwt'))
  @Post('imgFile')
  @UseInterceptors(FileInterceptor('file'))
  uploadImgFile(@UploadedFile() file) {
    console.log('上传图片', file);
    file.url = `${config.upload.domain}/upload/${file.filename}`;
    return { code: 200, msg: '图片上传成功', data: file };
  }
  /**
   * 多张上传图片
   */
  // @UseGuards(AuthGuard('jwt'))
  // @Post('imgFiles')
  // @UseInterceptors(FileInterceptor('file'))
  // uploadImgFiles(@UploadedFile() file) {
  //   console.log('上传图片', file);
  //   file.url = `${config.upload.domain}/upload/${file.filename}`;
  //   return { code: 200, msg: '图片上传成功', data: file };
  // }
}
