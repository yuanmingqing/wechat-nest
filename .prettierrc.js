// {
//   "singleQuote": true,
//   "trailingComma": "all"
// }

module.exports = {
  printWidth: 120, // 换行字符串阈值
  semi: true, // 句末加分号
  singleQuote: true, // 用单引号
  trailingComma: 'all', // 最后一个对象元素加逗号
  // bracketSpacing: false, // 对象，数组加空格
  // // 箭头函数参数括号 默认avoid 可选 avoid| always
  // // avoid 能省略括号的时候就省略 例如x => x
  // // always 总是有括号
  arrowParens: 'always', // (x) => {} 是否要有小括号
  requirePragma: false, // 是否要注释来决定是否格式化代码
  proseWrap: 'preserve', // 是否要换行
};