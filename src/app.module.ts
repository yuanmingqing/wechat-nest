import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

import { UserModule } from './modules/user/user.module';
import { FriendModule } from './modules/friend/friend.module';
import { DynamicModule } from './modules/dynamic/dynamic.module';
import { FileModule } from './modules/file/file.module';
import { AlbumModule } from './modules/album/album.module';
import { MusicModule } from './modules/music/music.modules';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '123456',
      database: 'wechat-nest',
      // entities: [],
      charset: 'utf8mb4', // 设置chatset编码为utf8mb4
      autoLoadEntities: true,
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    FileModule,
    DynamicModule,
    FriendModule,
    UserModule,
    AlbumModule,
    MusicModule,
  ],
})
export class AppModule {}
