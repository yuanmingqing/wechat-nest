import { Controller, Get, UseGuards, Req, Post, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserRegisterDto, UserLoginDto, UserDto } from './dto/index';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}
  @Post('login')
  async login(@Body() userLoginDto: UserLoginDto) {
    return this.userService.login(userLoginDto);
  }

  @Post('register')
  async register(@Body() userRegisterDto: UserRegisterDto) {
    return this.userService.register(userRegisterDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('info')
  async getUser(@Req() request) {
    // const req = context.switchToHttp().getRequest();
    const accessToken = request.get('Authorization');
    return this.userService.getUser(accessToken);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('getUserList')
  async getUserList(@Req() request) {
    const accessToken = request.get('Authorization');
    return this.userService.getUserList(accessToken);
  }

  /**
   * 修改用户信息
   */
  @UseGuards(AuthGuard('jwt'))
  @Post('updateUser')
  async updateUser(@Req() request, @Body() body: UserDto) {
    const accessToken = request.get('Authorization');
    return this.userService.updateUser(accessToken, body);
  }

  /**
   * 修改密码
   */
  @UseGuards(AuthGuard('jwt'))
  @Get('updatePwd')
  async updatePassword(@Req() request, @Body() body: UserDto) {
    const accessToken = request.get('Authorization');
    return this.userService.updatePassword(accessToken, body);
  }

  /**
   * 重置密码
   */
  @UseGuards(AuthGuard('jwt'))
  @Get('updatePwd')
  async resetPassword(@Body() body: { email: string; password: string; newPassword: string }) {
    return this.userService.resetPassword(body);
  }
}
