import { IsEmail, IsNotEmpty } from 'class-validator';
export class UserDto {
  readonly userId?: number;

  @IsNotEmpty()
  readonly username: string;

  readonly account?: string;

  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  readonly password: string;

  readonly sex: number;

  readonly birth: string;

  readonly phone?: string;

  readonly desc?: string;

  readonly imgUrl: string;

  readonly createTime?: string;

  readonly updateTime?: string;
}
