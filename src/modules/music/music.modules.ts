import { Module } from '@nestjs/common';

import { AuthModule } from '@auth/index';

import { MusicController } from './music.controller';
import { MusicPersonalizedController, MusicPlaylistController } from './controller/index';
import { MusicService } from './music.service';

@Module({
  imports: [AuthModule],
  controllers: [MusicController, MusicPersonalizedController, MusicPlaylistController],
  providers: [MusicService],
  exports: [MusicService],
})
export class MusicModule {}
