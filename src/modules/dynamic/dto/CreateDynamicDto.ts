export class CreateDynamicDto {
  readonly content: string;

  readonly imgUrl: string;

  readonly status: number;
}
