import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '@auth/index';
import { FriendEntity } from '@modules/friend/entity/friend.entity';
import { DynamicController } from './dynamic.controller';
import { DynamicService } from './dynamic.service';
import { DynamicEntity } from './entity/dynamic.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DynamicEntity, FriendEntity]), AuthModule],
  controllers: [DynamicController],
  providers: [DynamicService],
  exports: [DynamicService],
})
export class DynamicModule {}
