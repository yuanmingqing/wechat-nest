import { Controller, Get, Query } from '@nestjs/common';
import { playlist_detail, playlist_track_all } from 'NeteaseCloudMusicApi';
@Controller('music/playlist')
export class MusicPlaylistController {
  constructor() {}
  /**
   * 获取歌单详情
   * @param id: number | string; s?: string | number
   * @returns
   */
  @Get('detail')
  async playlistDetail(@Query() query: { id: number | string; s?: string | number }) {
    const { body } = await playlist_detail({ id: query.id, s: query.s });
    return { code: body.code, msg: '获取歌单详情', data: body || [] };
  }

  /**
   * 获取歌单所有歌曲
   * @param id: number | string; s?: string | number
   * @returns
   */
  @Get('track/all')
  async playlistTrackAll(@Query() query: { id: number | string; s?: string | number }) {
    const { body } = await playlist_track_all({ id: query.id, s: query.s });
    return { code: body.code, msg: '获取歌单所有歌曲', data: body || [] };
  }
}
