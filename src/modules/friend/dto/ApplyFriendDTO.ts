export class ApplyFriendDTO {
  readonly userId: number;

  readonly userName: string;

  readonly userUrl: string;

  readonly friendId: number;

  readonly friendName: string;

  readonly friendUrl: string;

  readonly status?: number;

  readonly msg: string;
}
