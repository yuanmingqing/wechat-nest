import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '@auth/index';
import { AlbumController } from './album.controller';
import { AlbumService } from './album.service';
import { AlbumEntity } from './entity/album.entity';
import { AlbumListEntity } from './entity/album.list.entity';

@Module({
  imports: [TypeOrmModule.forFeature([AlbumEntity, AlbumListEntity]), AuthModule],
  controllers: [AlbumController],
  providers: [AlbumService],
  exports: [AlbumService],
})
export class AlbumModule {}
