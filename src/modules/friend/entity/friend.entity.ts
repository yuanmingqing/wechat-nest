import * as dayjs from 'dayjs';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { UserEntity } from '@modules/user/entity/user.entity';

@Entity({ name: 'friends' })
export class FriendEntity {
  @PrimaryGeneratedColumn({
    comment: '主键id',
  })
  id: number;

  @Column({ comment: '用户id' })
  userId: number;

  @Column({ comment: '好友id' })
  friendId: number;

  @Column({
    default: dayjs().format('YYYY-MM-DD HH:mm:ss'), // 默认值
    comment: '创建时间',
  })
  createTime: string;

  @ManyToOne(() => UserEntity, (userEntity) => userEntity.friends)
  @JoinColumn({ name: 'friendId' })
  users: UserEntity;
}
