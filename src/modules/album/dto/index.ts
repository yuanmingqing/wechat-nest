export class CreateAlbumDto {
  readonly id?: number;

  readonly name: string;

  readonly desc: string;

  readonly imgUrl: string;
}

export class SavePhoneDto {
  readonly id?: number;

  readonly albumId: number;

  readonly name?: string;

  readonly imgUrl: string;

  readonly desc?: string;

  readonly address?: string;

  readonly createTime?: string;
}

export class UpdatePhoneDto {
  readonly id?: number;

  readonly albumId: number;

  readonly name?: string;

  readonly imgUrl: string;

  readonly desc?: string;

  readonly address?: string;

  readonly endTime?: string;
}
