export default {
  nest: {
    host: '127.0.0.1',
    port: 7001,
  },
  jwt: {
    secret: 'nest-chat', // fs.readFileSync('xxxxx.key')
    expiresIn: '20d', // https://github.com/vercel/ms
  },
  upload: {
    // mode: UploadMode, 默认为file，即上传到服务器临时目录，可以配置为 stream
    mode: 'file',
    // fileSize: string, 最大上传文件大小，默认为 10mb
    fileSize: '10mb',
    // whitelist: string[]，文件扩展名白名单
    // whitelist: uploadWhiteList.filter(ext => ext !== '.pdf'),
    whitelist: null, //uploadWhiteList.filter(ext => ext !== '.pdf')
    // tmpdir: string，上传的文件临时存储路径
    // tmpdir: join(tmpdir(), 'midway-upload-files'),
    // tmpdir: join(tmpdir(), 'public'),
    // cleanTimeout: number，上传的文件在临时目录中多久之后自动删除，默认为 5 分钟
    cleanTimeout: null,
    // base64: boolean，设置原始body是否是base64格式，默认为false，一般用于腾讯云的兼容
    base64: false,
    domain: 'http://127.0.0.1:7001',
  },
};
