import * as dayjs from 'dayjs';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { UserEntity } from '@modules/user/entity/user.entity';

@Entity({ name: 'dynamics' })
export class DynamicEntity {
  @PrimaryGeneratedColumn({
    comment: '主键id',
  })
  id: number;

  @Column({ comment: '用户id' })
  userId: number;

  @Column({
    comment: '动态内容',
  })
  content: string;

  @Column({
    comment: '动态图片',
  })
  imgUrl: string;

  @Column({
    comment: '查看状态 1:所有人可见 2: 仅自己可见',
  })
  status: number;

  @Column({
    default: dayjs().format('YYYY-MM-DD HH:mm:ss'), // 默认值
    comment: '创建时间',
  })
  createTime: string;

  @ManyToOne((type) => UserEntity)
  @JoinColumn({ name: 'userId' })
  users: UserEntity;
}
