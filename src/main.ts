// import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as compression from 'compression'; //使用压缩中间件启用 gzip 压缩
import config from '@config/config.default';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './common/filters/http-exception.filter';
import { logger } from './common/middleware/logger.middleware';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  // app.useGlobalPipes(new ValidationPipe());
  //cors：跨域资源共享，方式一：允许跨站访问
  // app.enableCors();
  // 方式二：const app = await NestFactory.create(AppModule, { cors: true });

  // 路径前缀：如：http://www.test.com/api/v1/user
  app.setGlobalPrefix('api/v1');
  // 全局中间件
  app.use(logger);
  // 全局过滤器
  app.useGlobalFilters(new HttpExceptionFilter());
  // somewhere in your initialization file
  app.use(compression());

  await app.listen(config.nest.port, () => {
    console.log(`服务已经启动,接口请访问=> ${config.nest.host}:${config.nest.port}`);
  });
}
bootstrap();
