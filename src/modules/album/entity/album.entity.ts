import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import * as dayjs from 'dayjs';

@Entity({ name: 'albums' })
export class AlbumEntity {
  @PrimaryGeneratedColumn({ comment: '主键id' })
  id: number;

  @Column({ comment: '用户id' })
  uid: number;

  @Column({ comment: '相册名称' })
  name: string;

  @Column({ comment: '相册封面' })
  imgUrl: string;

  @Column({ comment: '描述' })
  desc: string;

  @Column({
    default: dayjs().format('YYYY-MM-DD HH:mm:ss'), // 默认值
    comment: '创建时间',
  })
  createTime: string;

  @Column({
    default: dayjs().format('YYYY-MM-DD HH:mm:ss'), // 默认值
    comment: '结束时间',
  })
  endTime: string;
}
