import * as dayjs from 'dayjs';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';

// import { UserEntity } from '@modules/user/entity/user.entity';

@Entity({
  name: 'friend_apply',
})
export class FriendApplyEntity {
  @PrimaryGeneratedColumn({
    comment: '主键id',
  })
  id: number;

  @Column({
    comment: '用户id',
  })
  userId: number;

  @Column({
    comment: '用户名称',
  })
  userName: string;

  @Column({
    comment: '用户头像',
  })
  userUrl: string;

  @Column({
    comment: '好友id',
  })
  friendId: number;

  @Column({
    comment: '好友名称',
  })
  friendName: string;

  @Column({
    comment: '好友头像',
  })
  friendUrl: string;

  @Column({
    comment: '消息',
  })
  msg: string;

  @Column({
    comment: '状态',
  })
  status: number; // 1: 已经是好友；2: 申请添加好友；3: 拒绝添加好友

  @Column({
    default: dayjs().format('YYYY-MM-DD HH:mm:ss'), // 默认值
    comment: '创建时间',
  })
  createTime: string;
}
