import { Controller, Get, Post, Req, Body, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { DynamicService } from './dynamic.service';
import { CreateDynamicDto } from './dto/index';

@Controller('dynamic')
export class DynamicController {
  constructor(private dynamicService: DynamicService) {}
  /**
   * 发动态
   */
  @UseGuards(AuthGuard('jwt'))
  @Post('/createDynamic')
  async createDynamic(@Req() request, @Body() body: CreateDynamicDto) {
    const accessToken = request.get('Authorization');
    return this.dynamicService.createDynamic(accessToken, body);
  }

  /**
   * 获取全部动态
   */
  @UseGuards(AuthGuard('jwt'))
  @Get('/getAllDynamic')
  async getAllDynamic(@Req() request) {
    const accessToken = request.get('Authorization');
    return this.dynamicService.getAllDynamic(accessToken);
  }

  /**
   * 获取自己或别人动态
   */
  @UseGuards(AuthGuard('jwt'))
  @Get('/getDynamic')
  async getDynamic(@Req() request, @Query() body: { userId: number }) {
    const accessToken = request.get('Authorization');
    return this.dynamicService.getDynamic(accessToken, body.userId);
  }

  /**
   * 删除动态
   */
  @UseGuards(AuthGuard('jwt'))
  @Post('/deleteDynamic')
  async deleteDynamic(@Req() request, @Body() body: { id: number }) {
    const accessToken = request.get('Authorization');
    return this.dynamicService.deleteDynamic(accessToken, body.id);
  }

  /**
   * 动态评论
   */
  @UseGuards(AuthGuard('jwt'))
  @Post('/dynamicComment')
  async dynamicComment() {
    return this.dynamicService.dynamicComment();
  }

  /**
   * 获取动态评论
   */
  @UseGuards(AuthGuard('jwt'))
  @Get('/getDynamicComment')
  async getDynamicComment() {
    return this.dynamicService.getDynamicComment();
  }
}
