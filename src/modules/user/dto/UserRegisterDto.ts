import { IsEmail, IsNotEmpty } from 'class-validator';

export class UserRegisterDto {
  readonly username: string;

  @IsEmail()
  readonly email: string;

  readonly birth: string;

  readonly sex: number;

  @IsNotEmpty()
  readonly password: string;
}
